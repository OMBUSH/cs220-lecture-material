# Project 1 (P1)

## Clarifications/Corrections

None yet.

**Find any issues?** Report to us:

- Anna Meyer <apmeyer4@wisc.edu>
- Jane Zhang <zhang2752@wisc.edu>

## Learning Objectives

In this project, you will learn to:
* Run otter tests,
* Turn in your project using Gradescope.

## Note on Academic Misconduct:
P1 is the first project of CS220, and is the **only** project where you are **not** allowed to partner with anyone else. You **must** work on this project by yourself, and turn in your submission individually. Now may be a good time to review our [course policies](https://canvas.wisc.edu/courses/355767/pages/syllabus).

## Step 1: Setup

You should have a folder `cs220` that you created for lab-p1 under `Documents`. We're assuming you did the lab and know how to do all the Tasks we mentioned. If you're confused about any of the following steps, refer back to your lab.

#### Create a sub-folder called `p1` in your `cs220` folder
Refer to [Task 1.1](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/tree/main/sum23/labs/lab1#task-11-create-the-folders-for-this-lab) if you need help.  

This will store all your files related to P1. This way, you can keep files for different projects separate (you'll create a `p2` sub-folder for the next project and so on). Unfortunately, computers can crash and files can get accidentally deleted, so make sure you backup your work regularly (at a minimum, consider emailing yourself relevant files on occasion).

#### Download `p1.ipynb` to your `p1` folder.
Refer to [Task 1.7](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/tree/main/sum23/labs/lab1#task-17-correctly-download-a-python-file) if you need help.

#### Open a terminal in your `p1` folder.
Refer to [Task 1.3-1.5](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/tree/main/sum23/labs/lab1#task-13-open-a-terminal-emulator-aka-a-terminal).

#### Run `p1.ipynb` code using jupyter notebook
Refer to [Task 4.1](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/tree/main/sum23/labs/lab1#task-13-open-a-terminal-emulator-aka-a-terminal).
Make sure to follow the directions provided in `p1.ipynb`.

## Submission instructions
- Create a [Gradescope](https://www.gradescope.com/) login, if you haven't already created it.  **You must use your wisc email to create the Gradescope login**.
- Login to [Gradescope](https://www.gradescope.com/courses/531688/) and join our course -- see [Canvas](https://canvas.wisc.edu/courses/355767/pages/week-1?module_item_id=6048037) for the access code
- In gradescope, upload the zip file into the P1 assignment.
- It is **your responsibility** to make sure that your project clears auto-grader tests on the Gradescope test system. Otter test results should be available in a few minutes after your submission. You should be able to see both PASS / FAIL results for the 2 test cases and your total score, which is accessible via Gradescope Dashboard (as in the image below):
       
    <img src="images/gradescope.png" width="400">
